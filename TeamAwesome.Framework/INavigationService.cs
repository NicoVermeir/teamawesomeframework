﻿using System;

namespace TeamAwesome.Framework
{
    /// <summary>
    /// Allows page navigation in the app, RootFrame needs to be set in App.xaml
    /// </summary>
    public interface INavigationService
    {
        /// <summary>
        /// The rootframe for the app, this will take care of all the navigation
        /// </summary>
        object RootFrame { get; set; }

        /// <summary>
        /// true when there's a page on the backstack
        /// </summary>
        bool CanGoBack { get; }

        /// <summary>
        /// true when there's a page on the forwardstack
        /// </summary>
        bool CanGoForward { get; }

        /// <summary>
        /// go one step back on the backstack
        /// </summary>
        void GoBack();

        /// <summary>
        /// go one step forward on the stack
        /// </summary>
        void GoForward();

        /// <summary>
        /// resets the entire backstack
        /// </summary>
        void ResetBackstack();

        /// <summary>
        /// remove the last entry from the backstack
        /// </summary>
        void RemoveBackEntry();

        /// <summary>
        /// navigate to a page using the type of the page
        /// </summary>
        /// <param name="destination">the type of the page we want to navigate to</param>
        /// <param name="parameter">an optional parameter</param>
        /// <returns>true/false</returns>
        bool Navigate(Type destination, object parameter = null);

        /// <summary>
        /// navigate to a page using a uri string, paramers are passed using querystring
        /// </summary>
        /// <param name="uri">string pointing to the page we want to navigate too</param>
        /// <returns></returns>
        bool Navigate(string uri);

        /// <summary>
        /// navigate to a page using a uri, paramers are passed using querystring
        /// </summary>
        /// <param name="uri">uri we want to navigate too</param>
        /// <returns></returns>
        bool Navigate(Uri uri);
    }
}
