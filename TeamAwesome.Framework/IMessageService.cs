﻿using System.Threading.Tasks;

namespace TeamAwesome.Framework
{
    public interface IMessageService
    {
        /// <summary>
        /// Shows a messagebox containing the error
        /// </summary>
        /// <param name="errorMessage">the error message</param>
        /// <param name="caption">an optional parameter containing a caption</param>
        void ShowError(string errorMessage, string caption = "Error");

        /// <summary>
        /// A messagebox containing a yes/no question
        /// due to limitations in the WP SDK, the Windows Phone version will show OK and Cancel
        /// </summary>
        /// <param name="question">The question that should be asked</param>
        /// <param name="caption">an optional parameter containing a caption</param>
        /// <returns>true/false</returns>
        Task<bool> YesNoQuestion(string question, string caption = "Yes/No");
    }
}
