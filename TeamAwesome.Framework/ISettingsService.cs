﻿using System.Threading.Tasks;

namespace TeamAwesome.Framework
{
    public interface ISettingsService
    {
        /// <summary>
        /// Checks if a certain setting is already registered in the ApplicationSettings
        /// </summary>
        /// <param name="key">The key used to search for the setting</param>
        /// <returns>true/false</returns>
        Task<bool> DoesSettingExist(string key);

        /// <summary>
        /// Saves a setting in the ApplicationSettings dictionary
        /// </summary>
        /// <typeparam name="T">The type of the value</typeparam>
        /// <param name="key">The key for the setting</param>
        /// <param name="value">The setting's value, this is of type T</param>
        /// <returns></returns>
        Task SaveSetting<T>(string key, T value);

        /// <summary>
        /// Load a setting from the ApplicationSettings
        /// </summary>
        /// <typeparam name="T">Type of the setting</typeparam>
        /// <param name="key">The key for the setting</param>
        /// <returns>The saved setting or null</returns>
        Task<T> LoadSetting<T>(string key);
    }
}
