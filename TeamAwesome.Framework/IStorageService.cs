﻿using System.Threading.Tasks;

namespace TeamAwesome.Framework
{
    public interface IStorageService
    {
        /// <summary>
        /// saves a textfile to the isolated storage
        /// </summary>
        /// <param name="fileName">the name of the file</param>
        /// <param name="text">the content of the file</param>
        /// <returns></returns>
        Task SaveToIsoStore(string fileName, string text);

        /// <summary>
        /// loads the content of a textfile that is saved in IsolatedStorage
        /// </summary>
        /// <param name="fileName">the name of the file</param>
        /// <returns>the text in the file</returns>
        Task<string> LoadFromIsoStore(string fileName);
    }
}
