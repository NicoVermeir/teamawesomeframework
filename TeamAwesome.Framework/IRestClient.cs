﻿using System.Threading.Tasks;

namespace TeamAwesome.Framework
{
    public interface IRestClient
    {
        Task<T> Get<T>(string url) 
            where T : new();

        Task<bool> Post<T>(string url, T newObject);
    }
}