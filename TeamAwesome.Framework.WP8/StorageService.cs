﻿using System.IO;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;

namespace TeamAwesome.Framework.WP8
{
    public class StorageService : IStorageService
    {
        public async Task SaveToIsoStore(string fileName, string text)
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = store.OpenFile(fileName, FileMode.OpenOrCreate))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        await writer.WriteAsync(text);
                    }

                    //Dispose of the stream automatically created when creating the file. 
                   stream.Close();
                   stream.Dispose();
                }
            }
        }

        public async Task<string> LoadFromIsoStore(string fileName)
        {
            try
            {
                using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    try
                    {
                        using (IsolatedStorageFileStream stream = store.OpenFile(fileName, FileMode.Open))
                        {
                            //If the file isnt empty, then deserialize the xml into Settings objects and load them into the database.
                            if (stream.Length > 0)
                            {
                                using (StreamReader reader = new StreamReader(stream))
                                {
                                    return await reader.ReadToEndAsync();
                                }
                            }
                        }
                    }
                    catch (FileNotFoundException)
                    {
                        return "File not found";
                    }

                    return "";
                }
            }
            catch (IsolatedStorageException)
            {
                return "error";
            }
        }
    }
}
