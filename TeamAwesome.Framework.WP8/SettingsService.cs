﻿using System.IO.IsolatedStorage;
using System.Threading.Tasks;

namespace TeamAwesome.Framework.WP8
{
    public class SettingsService : ISettingsService
    {
        public async Task<bool> DoesSettingExist(string key)
        {
            return IsolatedStorageSettings.ApplicationSettings.Contains(key);
        }

        public async Task SaveSetting<T>(string key, T value)
        {
            if (await DoesSettingExist(key))
            {
                IsolatedStorageSettings.ApplicationSettings.Remove(key);
            }

            IsolatedStorageSettings.ApplicationSettings.Add(key, value);
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public async Task<T> LoadSetting<T>(string key)
        {
            try
            {
                return (T)IsolatedStorageSettings.ApplicationSettings[key];
            }
            catch (IsolatedStorageException)
            {
                return default(T);
            }
        }
    }
}