﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Phone.Controls;

namespace TeamAwesome.Framework.WP8
{
    public class NavigationService : INavigationService
    {
        private PhoneApplicationFrame _rootFrame;

        public bool CanGoBack { get { return _rootFrame.CanGoBack; } }
        public bool CanGoForward { get { return _rootFrame.CanGoForward; } }

        public object RootFrame
        {
            get { return _rootFrame; }
            set
            {
                _rootFrame = (PhoneApplicationFrame)value;
            }
        }

        public void GoBack()
        {
            if (!_rootFrame.CanGoBack) return;

            _rootFrame.GoBack();
        }

        public void ResetBackstack()
        {
           _rootFrame.BackStack.GetEnumerator().Reset();
        }

        public void RemoveBackEntry()
        {
            _rootFrame.RemoveBackEntry();            
        }

        public void GoForward()
        {
            if (!_rootFrame.CanGoForward) return;

            _rootFrame.GoForward();
        }

        public bool Navigate(Type destination, object parameter = null)
        {
            try
            {
                string fqname = destination.FullName;
                var path = Regex.Split(fqname, @"\.");

                string destinationUri = string.Empty;

                for (int i = 1; i < path.Length; i++)
                {
                    destinationUri = destinationUri + "/" + path[i];
                }

                _rootFrame.Navigate(new Uri(destinationUri + ".xaml", UriKind.Relative));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Navigate(string uri)
        {
            try
            {
                _rootFrame.Navigate(new Uri(uri, UriKind.Relative));

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Navigate(Uri uri)
        {
            try
            {
                _rootFrame.Navigate(uri);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
