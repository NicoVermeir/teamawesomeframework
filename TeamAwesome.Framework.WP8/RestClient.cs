﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TeamAwesome.Framework.WP8
{
    public class RestClient : IRestClient
    {
        public async Task<T> Get<T>(string url) 
            where T : new()
        {
            try
            {
                var handler = new HttpClientHandler();
                if (handler.SupportsAutomaticDecompression)
                {
                    handler.AutomaticDecompression = DecompressionMethods.GZip |
                                                     DecompressionMethods.Deflate;
                }

                var httpClient = new HttpClient(handler);
                var result = await httpClient.GetStringAsync(url);
                return await JsonConvert.DeserializeObjectAsync<T>(result);
            }
            catch (HttpRequestException)
            {
                return new T();
            }
            catch (Exception)
            {
                return new T();
            }
        }

        public async Task<bool> Post<T>(string url, T newObject)
        {
            try
            {
                var handler = new HttpClientHandler();
                if (handler.SupportsAutomaticDecompression)
                {
                    handler.AutomaticDecompression = DecompressionMethods.GZip |
                                                        DecompressionMethods.Deflate;
                }

                HttpContent content = new StringContent(JsonConvert.SerializeObject(newObject));
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var httpClient = new HttpClient(handler);
                HttpResponseMessage response = await httpClient.PostAsync(url, content);
                
                return response.StatusCode == HttpStatusCode.Created;
            }
            catch (HttpRequestException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
