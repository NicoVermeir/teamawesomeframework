﻿using System.Threading.Tasks;
using System.Windows;

namespace TeamAwesome.Framework.WP8
{
    public class MessageService : IMessageService
    {
        public void ShowError(string errorMessage, string caption = "Error")
        {
            MessageBox.Show(errorMessage, caption, MessageBoxButton.OK);
        }

        public async Task<bool> YesNoQuestion(string question, string caption = "Yes/No")
        {
            return MessageBox.Show(question, caption, MessageBoxButton.OKCancel) == MessageBoxResult.OK;
        }
    }
}
